        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta
            name="description"
            content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
        <meta
            name="keywords"
            content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="PIXINVENT">
        <title>Yayasan Hang Tuah</title>
        <link
            rel="apple-touch-icon"
            href="app-assets/images/ico/apple-icon-120.png">
        <link
            rel="shortcut icon"
            type="image/x-icon"
            href="app-assets/images/ico/Hang Tuah Logo 2.svg">
        <link
            href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600"
            rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link
            rel="stylesheet"
            type="text/css"
            href="app-assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link
            rel="stylesheet"
            type="text/css"
            href="app-assets/css/bootstrap.css">
        <link
            rel="stylesheet"
            type="text/css"
            href="app-assets/css/bootstrap-extended.css">
        <link
            rel="stylesheet"
            type="text/css"
            href="app-assets/css/colors.css">
        <link
            rel="stylesheet"
            type="text/css"
            href="app-assets/css/components.css">

        <!-- BEGIN: Page CSS-->
        <link
            rel="stylesheet"
            type="text/css"
            href="app-assets/css/core/menu/menu-types/vertical-menu.css">
        <link
            rel="stylesheet"
            type="text/css"
            href="app-assets/css/core/colors/palette-gradient.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <!-- END: Custom CSS-->
        <style>
            .table-borderless > tbody > tr > td,
            .table-borderless > tbody > tr > th,
            .table-borderless > tfoot > tr > td,
            .table-borderless > tfoot > tr > th,
            .table-borderless > thead > tr > td,
            .table-borderless > thead > tr > th {
                border: none;
            }
        </style>