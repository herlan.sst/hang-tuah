<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->

    <head>
        <?php include 'header.php';?>
    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->

    <body
        class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static  "
        data-open="click"
        data-menu="vertical-menu-modern"
        data-col="2-columns">

        <!-- BEGIN: Header-->
        <?php include 'top.php'; ?>
        <!-- END: Header-->
        
        <!-- BEGIN: Main Menu-->
        <?php include 'menu.php'; ?>
        
        <!-- END: Main Menu-->

        <!-- BEGIN: Content-->
        <?php include 'redirect.php'; ?>
        <!-- END: Content-->

        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        <!-- BEGIN: Footer-->
        <?php include 'footer.php'; ?>
        <!-- END: Footer-->
        
        <?php include 'js.php'; ?>
        

    </body>
    <!-- END: Body-->

</html>