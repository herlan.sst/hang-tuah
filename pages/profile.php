        <div class="app-content content">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-header row">
                    <div class="content-header-left col-md-9 col-12 mb-2">
                        <div class="row breadcrumbs-top">
                            <div class="col-12">
                                <h2 class="content-header-title float-left mb-0">Profile</h2>
                                <div class="breadcrumb-wrapper col-12">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item">
                                            <a href="index.html">Home</a>
                                        </li>
                                       
                                        <li class="breadcrumb-item active">Profile
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6" >
                                                <fieldset class="form-group position-relative has-icon-left input-divider-left">
                                                    <input type="text" class="form-control" id="iconLeft3" placeholder="Search">
                                                    <div class="form-control-position">
                                                        <i class="fa fa-search"></i>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-3">
                                                
                                                <fieldset class="form-group position-relative input-divider-right">
                                                    <select class="form-control">
                                                        <option value="">Filter By</option>
                                                        <option value="Jenis Kelamin">Jenis Kelamin</option>
                                                        <option value="Tahun Masuk">Tahun Masuk</option>
                                                    </select>
                                                    <div class="form-control-position">
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Content types section start -->
                    <section id="content-types">
                        <div class="row">
                            <div class="col-xl-6 col-md-6 col-sm-12">
                                <h4 style="color: blue;">Hang Tuah Profile</h4>
                                <div class="card" style="overflow: auto; height: 40rem !important;">
                                    <div class="card-content">
                                        <div class="card-body" style="padding:20px">
                                            <table style="background-color: #dce6f2; font-size: 12px; font-weight: bold;" class="table table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td rowspan=5> <img src="assets/images/guru.png" alt="Girl in a jacket"
                                                                style="width:150px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nama</td>
                                                        <td>:</td>
                                                        <td>Amri Husainy</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jabatan</td>
                                                        <td>:</td>
                                                        <td>Ketua Umum Pengurus YTH</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan=3>3-01</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan=3 align="right">
                                                            <button type="button" class="btn btn-primary waves-effect waves-light">Details
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <br>
                                            <table style="background-color: #dce6f2; font-size: 12px; font-weight: bold;" class="table table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td rowspan=5> <img src="assets/images/guru.png" alt="Girl in a jacket"
                                                                style="width:150px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nama</td>
                                                        <td>:</td>
                                                        <td>Amri Husainy</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jabatan</td>
                                                        <td>:</td>
                                                        <td>Ketua Umum Pengurus YTH</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan=3>3-01</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan=3 align="right">
                                                            <button type="button" class="btn btn-primary waves-effect waves-light">Details
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <br>
                                            <table style="background-color: #dce6f2; font-size: 12px; font-weight: bold;" class="table table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td rowspan=5> <img src="assets/images/guru.png" alt="Girl in a jacket"
                                                                style="width:150px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nama</td>
                                                        <td>:</td>
                                                        <td>Amri Husainy</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jabatan</td>
                                                        <td>:</td>
                                                        <td>Ketua Umum Pengurus YTH</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan=3>3-01</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan=3 align="right">
                                                            <button type="button" class="btn btn-primary waves-effect waves-light">Details
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-6 col-sm-12">
                                <h4 style="color: blue;">Preview</h4>
                                <h4 style="color: rgb(5, 5, 10);">Kartu Tanda Anggota Yayasan Hang Tuah</h4>
                                <div class="row">
                                    <div class="col-xl-6 col-md-6 col-sm-12">
                                        <div class="card" style="background-color: #dce6f2;">
                                            <div class="card-content">
                                                <div class="card-body" style="padding:20px">
                                                    <h6 align="center" style="background-color: white;">Tampak Muka</h6><br>
                                                    <table style="background-color: #dce6f2; font-size: 12px; " class="w-100">
                                                        <tr>
                                                            <td align="center"> <img src="assets/images/Hang Tuah Logo 2.svg"
                                                                    alt="Girl in a jacket" style="width:70px"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center"> <img src="assets/images/guru2.png" alt="Girl in a jacket"
                                                                    style="width: 100px; height: 115px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <h6 align="center" font-size: 14px;>1-05</h6>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center"> <img src="assets/images/qrcode.png" alt="Girl in a jacket"
                                                                    style="width:60px"></td>
                                                        </tr>
                                                        <tr>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 col-sm-12">
                                        <div class="card" style="background-color: #dce6f2;">
                                            <div class="card-content" style="background-image: url('assets/images/Hang Tuah Logo 3.svg'); background-size: 200px;
                                            background-repeat: no-repeat;
                                            background-position: center;">
                                                <div class="card-body" style="padding:20px">
                                                    <h6 align="center" style="background-color: white;">Tampak Belakang</h6><br>
                                                    <table style="font-size: 6px; " class="w-100">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 30%;">

                                                                </th>
                                                                <th>
                                                                    
                                                                </th>
                                                                <th>
                                                                    
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <p  align="center">KARTU TANDA ANGGOTA</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: top;" >Nama</td>
                                                                <td style="vertical-align: top;" >:</td>
                                                                <td style="vertical-align: top;" >Drs. Haidir MAP</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: top;" >Jabatan</td>
                                                                <td style="vertical-align: top;" >:</td>
                                                                <td style="vertical-align: top;" >Anggota Pembina Bidang Umum YHT
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: top;" >T/Tgl Lahir</td>
                                                                <td style="vertical-align: top;" >:</td>
                                                                <td style="vertical-align: top;" >Jakarta/19-01-1959</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: top;" >Alamat</td>
                                                                <td style="vertical-align: top;" >:</td>
                                                                <td style="vertical-align: top;" >Pondok Griya Jatimurni Blok A.11,
                                                                    Pondok
                                                                    Melati, Kota Bekasi Jawa Barat </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: top;" >Agama</td>
                                                                <td style="vertical-align: top;" >:</td>
                                                                <td style="vertical-align: top;" >Islam </td>
                                                            </tr>
                                
                                                            <tr>
                                                                <td style="vertical-align: top; " >Gol. Darah</td>
                                                                <td style="vertical-align: top;" >:</td>
                                                                <td style="vertical-align: top;" >O </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: top;" >Masa Berlaku</td>
                                                                <td style="vertical-align: top;" >:</td>
                                                                <td style="vertical-align: top;" >19-12-2030  </td>
                                                            </tr>
                                                        </tbody>
                                                        <table style=" font-size: 6px; text-align: center; margin-top: 10px;" class="w-100">
                                                            <tr>
                                                                <td style="vertical-align: top;" >Tanda tangan <br> Pemegang</td>
                            
                                                                <td style="vertical-align: top;">Jakarta, 23-12-20 </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: top;"> <br> <br>....................</td>
                                                                <td style="vertical-align: top;"> <br> <br>....................</td>
                                                            </tr>
                                                        </table>
                                                    </table>
                                                    <span style="font-size: 6px;"> Keterangan:</span>
                                                    <ol style="font-size: 6px;">
                                                    <li>
                                                        Kartu tanda anggota YHT digunakan sebagaimana mestinya
                                                    </li> 
                                                    <li>
                                                        Kehilangan atau menemukan kartu ini harap menghubungi sekretaris YHT Jalan Tabah Raya No. 2 Komplek TNI AL Kodamar Kelapa Gading - Jakarta Utara 14240 <br>
                                                        Telp: 021-4500718
                                                    </li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-12 col-md-12 col-sm-12 text-center">  <td colspan=3 align="right">
                                        <button type="button" class="btn btn-primary  mr-1 mb-1 waves-effect waves-light">Print
                                    </td></div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>