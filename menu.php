        <div
            class="main-menu menu-fixed menu-light menu-accordion menu-shadow"
            data-scroll-to-active="true">
            <div class="navbar-header" style="height: 8rem;">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mr-auto">
                        <a
                        class="navbar-brand" style="margin: 10px 0px 0px 60px;"
                        href="?page=home">
                        <div><img
                            src="assets/images/Hang Tuah Logo 2.svg"
                            style="width:95px"></div>
                      
                    </a>
                    </li>
                   
                </ul>
            </div>
            <div class="shadow-bottom"></div>
            <div class="main-menu-content" >
                <ul
                    class="navigation navigation-main"
                    id="main-menu-navigation"
                    data-menu="menu-navigation"
                   >
                   <?php
                        $page = (isset($_GET['page']))? $_GET['page'] : '';
                    ?>
                    <li class=" nav-item <?=$page=='home'||$page==''?'active':''?>" >
                        <a href="?page=home">
                            <i class="feather icon-home"></i>
                            <span class="menu-title" data-i18n="Dashboard" >Home</span></a>
                    </li>

                    <li class=" nav-item <?=$page=='profile'?'active':''?>"  >
                        <a href="?page=profile ">
                            <i class="fa fa-user"></i>
                            <span class="menu-title" data-i18n="Card">Profle</span></a>
                    </li>
                    <!-- <li class=" nav-item">
                        <a href="#">
                            <i class="fa fa-user"></i>
                            <span class="menu-title" data-i18n="Card">Profle</span></a>
                        <ul class="menu-content">
                            <li>
                                <a href="?page=profile">
                                    <i class="feather icon-circle"></i>
                                    <span class="menu-item" data-i18n="Basic">List Guru</span></a>
                            </li>
                        </ul>
                    </li> -->

                </ul>
            </div>
        </div>